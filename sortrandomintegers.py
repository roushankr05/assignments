# The input is a string of 1 million integers e.g "2 3 9 21 13 99 ... "
def SortRandom(string_input):
    input_list = string_input.split() #splits the input string on spaces
    # process string elements in the list and make them integers
    input_list = [int(a) for a in input_list]
    input_list = countSort(input_list)
    print input_list


# A function to do counting sort of arr[]
def countSort(arr):

    # The output  array that will have sorted arr
    output = [0 for i in range(256)]

    # Create a count array to store count of inidividul
    # characters and initialize count array as 0
    count = [0 for i in range(256)]

    ans = ["" for _ in arr]

    # Store count of each character
    for i in arr:
        count[i] += 1

    # Change count[i] so that count[i] now contains actual
    # position of this character in output array
    for i in range(256):
        count[i] += count[i-1]

    # Build the output character array
    for i in range(len(arr)):
        output[count[arr[i]]-1] = arr[i]
        count[arr[i]] -= 1

    # Copy the output array to arr, so that arr now
    # contains sorted characters
    for i in range(len(arr)):
        ans[i] = output[i]
    return ans



SortRandom("2 3 4 5 6 122 7 8 12 1 10")



