# A is the array and n is the number by which it has to be rotated
def RotateArray(A,n):
    if n <= 0 :
        return A
    len_of_array = len(A)
    # reverse part of array upto length od arrray - n elements
    A[0:len_of_array-n] = A[0:len_of_array-n][::-1]
    # reverse the remaining part of array
    A[len_of_array-n:len_of_array]= A[len_of_array-n:len_of_array][::-1]
    # now reverse the whole array
    A.reverse()
    return A


b = [1, 2, 3, 4, 5, 6]
b = RotateArray(b,5)


