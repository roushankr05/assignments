class GenarateDbDiff(object):

    def __init__(self,dicta,dictb):
        self.dicta = dicta
        self.dictb = dictb
        self.fmisprod = {}  # fields of table present in stage but not in prod
        self.fmisstage = {}  # fields of table present in prod but not in stage
        self.tbmisprod = {}  # tables present in stage but not in prod db
        self.tbmisstage = {}  # tables present in prod but not in stage db
        self.diffields = {}  # fields present in both the tables but modified
        self.PrStageNtProd()
        self.PrProdNtStage()
        self.FldMissing()
        self.FldPrTypMissing()
        self.GenerateSql()

    def Compare(self,table):
        fieldsfirst = set("".join(j[0]) for j in self.dicta[table])
        fieldssecond = set("".join(j[0]) for j in self.dictb[table])
        self.fmisprod[table] =  list(fieldsfirst - fieldssecond)
        self.fmisstage[table] =  list(fieldssecond - fieldsfirst)
        if self.fmisprod[table] == []:#delete emplty lists
            del self.fmisprod[table]
        if self.fmisstage[table] == []:
            del self.fmisstage[table]

    def CompareFields(self,tablename,stage,prod):
        res = []
        indexa = ["".join(p[0]) for p in stage]
        indexb = ["".join(p[0]) for p in prod]
        for count, i in enumerate(indexa):
            try:
                indb = indexb.index(i)
                if stage[count] != prod[indb]:
                    res.append(stage[count])
            except ValueError:
                continue
        self.diffields[tablename] = res

    def PrStageNtProd(self):
        # tables present in stage but not in prod db
        for i in self.dicta:
            if i not in self.dictb:
                self.tbmisprod[i]= self.dicta[i]
        for i in self.tbmisprod:
            del self.dicta[i]
    def PrProdNtStage(self):
        # tables present in prod but not in stage db
        for i in self.dictb:
            if i not in self.dicta:
                self.tbmisstage[i]= self.dictb[i]
        for i in self.tbmisstage:
            del self.dictb[i]
    def FldMissing(self):
        #fields of table present in stage but not in prod and vice versa
        for table in self.dicta:
            self.Compare(table)
    def FldPrTypMissing(self):
        # fields of table present in both but types are  different
        for tablename in self.dicta:
            self.CompareFields(tablename,self.dicta[tablename],self.dictb[tablename])

    def GenerateSql(self):
        f = open("output.sql",'w')
        #create tables present in stage but not in prod
        if self.tbmisprod != {}:
            for table in  self.tbmisprod:
                statement = "create table"
                statement += " " + table + " ( "
                for i in self.tbmisprod[table]:
                    statement +=  i[0] + ' ' + i[1]
                    if "PRI" in i :
                        statement+= " Primary key,"
                    elif "Foreign" in i :
                        statement += " Foreign key,"
                    else:
                        statement+= ","
                statement = statement.strip(",")
                statement += ');'
                f.write(statement)
                f.write("\n")
                statement =""

        #delete tables present in prod but not stage
        if self.tbmisstage != {}:
            for table in self.tbmisstage:
                statement = "Drop table "
                statement+= table  + ';'
                f.write(statement)
                f.write("\n")
                statement = " "

        #Add fields present in stage but missing in production
        if self.fmisprod !={}:
            for table in self.fmisprod:
                for field in self.fmisprod[table]:
                    for i in dicta[table]:
                        if i[0] == field:
                            statement = "Alter table " + table + ' add ' + field + ' '+ i[1]+''
                            if i[3]=="PRI":
                                statement += " Primary key ;"
                            elif i[3]=="FK":
                                statement+= " Foreign Key ;"
                            else:
                                statement += ';'
                            f.write(statement)
                            f.write("\n")

        #Delete fields present in prod but not in stage
        for table in self.fmisstage:
            for fields in self.fmisstage[table]:
                statement = "Alter table " + table +' drop ' + fields+";"
            f.write(statement)
            f.write("\n")

        #modify fields which are of different type
        for table in self.diffields:
            for fields in self.diffields[table]:
                statement =  "ALter table " + table + " modify column " + fields[0] + ' ' + fields[1]
                if fields[3] == "PRI":
                    statement+= " Primary key;"
                elif fields[3] =="Fk":
                    statement+= "Foreign Key;"
                else:
                    statement+=';'
                f.write(statement)
                f.write("\n")
        f.close()

dicta = {u'dept': [(u'name', u'varchar(10)', u'YES', u'PRI', None, u''), (u'branch', u'varchar(15)', u'YES', u'', None, u''), (u'city', u'varchar(10)', u'YES', u'', None, u'')],u'person': [(u'friend', u'varchar(5)', u'NO', u'', None, u''),(u'sno', u'varchar(5)', u'NO', u'PRI', None, u''), (u'name', u'varchar(30)', u'YES', u'', None, u''), (u'age', u'int(3)', u'YES', u'', None, u''), (u'sex', u'varchar(1)', u'YES', u'FK', None, u'')], u'office': [(u'name', u'varchar(18)', u'YES', u'', None, u''), (u'location', u'varchar(15)', u'YES', u'', None, u''), (u'city', u'varchar(15)', u'YES', u'', None, u'')]}
dictb = {u'social': [(u'nickname', u'varchar(5)', u'NO', u'PRI', None, u''),(u'sno', u'varchar(4)', u'NO', u'PRI', None, u''), (u'name', u'varchar(30)', u'YES', u'', None, u''), (u'age', u'int(3)', u'YES', u'', None, u'')],u'person': [(u'nickname', u'varchar(5)', u'NO', u'PRI', None, u''),(u'sno', u'varchar(4)', u'NO', u'PRI', None, u''), (u'name', u'varchar(30)', u'YES', u'', None, u''), (u'age', u'int(3)', u'YES', u'', None, u'')], u'office': [(u'name', u'varchar(10)', u'YES', u'', None, u''), (u'location', u'varchar(15)', u'YES', u'', None, u''), (u'city', u'varchar(10)', u'YES', u'', None, u'')]}
a = GenarateDbDiff(dicta,dictb)
print a.tbmisprod
print a.tbmisstage
print a.fmisprod
print a.fmisstage
print a.diffields
print "hie"
